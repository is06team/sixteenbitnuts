﻿using Microsoft.Xna.Framework;

namespace SixteenBitNuts
{
    public struct Tile
    {
        public string Index;
        public Point Position;
        public int? OverrideLayer;
    }
}
