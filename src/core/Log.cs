﻿using System;

namespace SixteenBitNuts
{
    public static class Log
    {
        enum LogColor
        {
            Red,
            Yellow,
            Blue,
            Green,
        }

        public static void success(string text)
        {
            PrintSign("INFO", LogColor.Green);

            Console.WriteLine(text);

            ResetColors();
        }

        public static void info(string text)
        {
            PrintSign("INFO", LogColor.Blue);

            Console.WriteLine(text);

            ResetColors();
        }

        public static void warning(string text)
        {
            PrintSign("WARN", LogColor.Yellow);

            Console.WriteLine(text);

            ResetColors();
        }

        public static void error(string text)
        {
            PrintSign("ERROR", LogColor.Red);

            Console.WriteLine(text);

            ResetColors();
        }

        private static void PrintSign(string text, LogColor color)
        {
            var backgroundColor = color switch
            {
                LogColor.Blue => ConsoleColor.DarkCyan,
                LogColor.Yellow => ConsoleColor.DarkYellow,
                LogColor.Red => ConsoleColor.DarkRed,
                LogColor.Green => ConsoleColor.DarkGreen,
                _ => throw new NotImplementedException()
            };

            var foregroundColor = color switch
            {
                LogColor.Blue => ConsoleColor.Cyan,
                LogColor.Yellow => ConsoleColor.Yellow,
                LogColor.Red => ConsoleColor.Red,
                LogColor.Green => ConsoleColor.Green,
                _ => throw new NotImplementedException()
            };

            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(text);

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = foregroundColor;
            Console.Write(" ");
        }

        private static void ResetColors()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
