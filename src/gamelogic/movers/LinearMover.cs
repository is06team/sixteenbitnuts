﻿using Microsoft.Xna.Framework;

namespace SixteenBitNuts
{
    public class LinearMover : Mover
    {
        public LinearMover(Vector2 directionVector, float speed = 1.0f) : base()
        {
            MoveFunction = (float t) => directionVector * speed;
        }

        public LinearMover(float directionAngle, float speed = 1.0f) : base()
        {
            var vector = VectorHelper.FromAngle(directionAngle);
            MoveFunction = (float t) => vector * speed;
        }

        public LinearMover(Direction direction, float speed = 1.0f) : base()
        {
            var vector = direction.GetVector();
            MoveFunction = (float t) => vector * speed;
        }
    }
}
