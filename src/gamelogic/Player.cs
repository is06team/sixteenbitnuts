﻿using Microsoft.Xna.Framework;

namespace SixteenBitNuts
{
    /// <summary>
    /// Represents all kind of player in games
    /// </summary>
    public abstract class Player : Actor
    {
        public delegate void LifeCycleStepHandler();

        public event CollisionSideHandler? OnCollideVertically;
        public event CollisionSideHandler? OnCollideHorizontally;

        public event LifeCycleStepHandler? OnBeforeUpdateVelocity;
        public event LifeCycleStepHandler? OnAfterUpdateVelocity;

        public event LifeCycleStepHandler? OnBeforeApplyMoveAndDetectCollisions;
        public event LifeCycleStepHandler? OnAfterApplyMoveAndDetectCollisions;

        // Public fields (for authoring edit)
        public float RunSpeed;
        public float RunAcceleration = 0.04f;
        public float RunMaxAcceleration = 0.4f;
        public float RunDeceleration = 0.1f;
        public Vector2 Velocity;

        // Properties
        public bool IsControllable { get; set; }
        public bool IsRunning { get; protected set; }
        public Direction MoveDirection { get; set; }
        public Direction LookDirection { get; set; }

        // Internal fields
        protected VirtualStick? runStick;
        protected bool isIntersectingWithObstacle = false;

        /// <summary>
        /// Creates a player for the given map
        /// </summary>
        /// <param name="map">Map in which the player will be created</param>
        public Player(Map map, Point? hitBoxSize = null) : base(map, hitBoxSize)
        {
            IsControllable = true;
            IsCollisionWithSolidsEnabled = true;
            RunSpeed = 1;
            LookDirection = Direction.Right;
        }

        /// <summary>
        /// Updates all data for this player
        /// </summary>
        public override void Update(GameTime time)
        {
            base.Update(time);

            runStick?.Update();

            // Update direction phase
            UpdateMoveDirection();

            // Update velocity
            OnBeforeUpdateVelocity?.Invoke();
            UpdateVelocity();
            OnAfterUpdateVelocity?.Invoke();

            // Update state phase
            UpdateStates();

            // Update sprite phase
            UpdateSprite();

            // Apply move phase
            OnBeforeApplyMoveAndDetectCollisions?.Invoke();
            ApplyMoveAndDetectCollisions();
            OnAfterApplyMoveAndDetectCollisions?.Invoke();
        }

        /// <summary>
        /// Override this function to define how the direction is updated
        /// </summary>
        protected abstract void UpdateMoveDirection();

        /// <summary>
        /// Override this function to define how the velocity is updated
        /// </summary>
        protected abstract void UpdateVelocity();

        /// <summary>
        /// Override this function to define states for the player after velocity computation
        /// </summary>
        protected virtual void UpdateStates()
        {

        }

        protected virtual void UpdateSprite()
        {

        }

        /// <summary>
        /// Performs the movement of the player according to its velocity
        /// </summary>
        private void ApplyMoveAndDetectCollisions()
        {
            isIntersectingWithObstacle = false;

            if (Velocity != Vector2.Zero)
            {
                MoveX(Velocity.X, (Solid solid) =>
                {
                    isIntersectingWithObstacle = true;

                    if (Velocity.X > 0)
                    {
                        OnCollideHorizontally?.Invoke(CollisionSide.Left);
                    }
                    else if (Velocity.X < 0)
                    {
                        OnCollideHorizontally?.Invoke(CollisionSide.Right);
                    }
                });
                MoveY(Velocity.Y, (Solid solid) =>
                {
                    isIntersectingWithObstacle = true;

                    if (Velocity.Y > 0)
                    {
                        OnCollideVertically?.Invoke(CollisionSide.Top);
                    }
                    else if (Velocity.Y < 0)
                    {
                        OnCollideVertically?.Invoke(CollisionSide.Bottom);
                    }
                });
            }
        }
    }
}
