﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SixteenBitNuts
{
    public class PlatformerPlayer : Player
    {
        public float Weight = 1f;
        public float JumpForce = -8f;
        public float AirDamping = 0.05f;
        public float GroundDamping = 0.25f;

        public bool IsJumping { get; private set; }
        public bool IsFalling { get; private set; }
        public bool IsTouchingTheGround { get; private set; }
        public bool IsTouchingTheCeiling { get; private set; }

        public float DebugRunHorizontalVelocityFactor { get { return horizontalVelocityFactor; } }
        public float HorizontalDamping { get { return horizontalDirection; } }
        public float TargetHorizontal { get { return targetHorizontalDirection; } }

        protected VirtualButton jumpButton;

        private bool isJumpButtonPressed = false;
        private bool wasOnTheGround = false;

        protected float horizontalVelocityFactor;
        private float horizontalDirection = 1;
        private float targetHorizontalDirection = 1;
        private int targetHorizontalCoolDown;

        private const int targetHozitontalCoolDownFrames = 5;

        public PlatformerPlayer(Map map, Point? hitBoxSize = null) : base(map, hitBoxSize)
        {
            // Properties
            GroundDamping = 0.25f;

            // Controls
            runStick = new VirtualStick(map.Game)
                .AddKeys(Keys.Left, Keys.Right, Keys.Up, Keys.Down)
                .AddButtons(PlayerIndex.One, Buttons.DPadLeft, Buttons.DPadRight, Buttons.DPadUp, Buttons.DPadDown);
            jumpButton = new VirtualButton()
                .AddKey(Keys.C)
                .AddButton(PlayerIndex.One, Buttons.A);

            // Update lifecycle events
            OnBeforeUpdateVelocity += PlatformerPlayer_UpdateHorizontalMove;
            OnBeforeApplyMoveAndDetectCollisions += PlatformerPlayer_ApplyGravity;
            OnAfterApplyMoveAndDetectCollisions += PlatformerPlayer_UpdatePlatformRideState;

            // Collision events
            OnCollideVertically += PlatformerPlayer_SetGroundAndCeilingStates;
        }

        protected override void UpdateMoveDirection()
        {
            if (IsControllable)
            {
                if (runStick is VirtualStick stick)
                {
                    MoveDirection = DirectionHelper.FromNormalizedHorizontal((int)stick.Value.X);
                    if (MoveDirection != Direction.None)
                    {
                        LookDirection = MoveDirection;
                    }
                }
            }
        }

        /// <summary>
        /// Just before updating velocity, we compute horizontal move
        /// </summary>
        private void PlatformerPlayer_UpdateHorizontalMove()
        {
            bool isRunning = false;

            if (MoveDirection == Direction.Left)
            {
                targetHorizontalDirection = -1;
                isRunning = true;

            }
            else if (MoveDirection == Direction.Right)
            {
                targetHorizontalDirection = 1;
                isRunning = true;
            }
            else
            {
                targetHorizontalDirection = 0;
            }

            if (isRunning) // Player is pressing direction
            {
                targetHorizontalCoolDown = targetHozitontalCoolDownFrames;

                // Accelerate until max accel
                if (horizontalVelocityFactor < RunMaxAcceleration)
                    horizontalVelocityFactor += RunAcceleration;

                // Apply ground friction
                if (!IsJumping && !IsFalling)
                {
                    if (horizontalDirection < targetHorizontalDirection)
                    {
                        horizontalDirection += GroundDamping;
                    }
                    if (horizontalDirection > targetHorizontalDirection)
                    {
                        horizontalDirection -= GroundDamping;
                    }
                }
            }
            else // Player is releasing direction
            {
                targetHorizontalCoolDown--;
                if (targetHorizontalCoolDown <= 0)
                {
                    targetHorizontalDirection = 0;
                    targetHorizontalCoolDown = 0;
                }

                if (!IsJumping && !IsFalling)
                {
                    if (targetHorizontalCoolDown <= 0)
                        horizontalDirection = 0;

                    // Decelerate until 0
                    if (horizontalVelocityFactor > 0)
                        horizontalVelocityFactor -= RunDeceleration;
                    else
                        horizontalVelocityFactor = 0;
                }
            }

            // Apply air friction
            if (IsJumping || IsFalling)
            {
                if (horizontalDirection < targetHorizontalDirection)
                {
                    horizontalDirection += AirDamping;
                }
                if (horizontalDirection > targetHorizontalDirection)
                {
                    horizontalDirection -= AirDamping;
                }
            }
        }

        /// <summary>
        /// Updates the player velocity during different stages like running or jumping
        /// </summary>
        protected override void UpdateVelocity()
        {
            if (IsControllable)
            {
                UpdateRunVelocity();
                UpdateJumpVelocity();
            }
        }

        private void UpdateRunVelocity()
        {
            // Direction limits
            if (horizontalDirection < -1) horizontalDirection = -1;
            if (horizontalDirection > 1) horizontalDirection = 1;

            // Apply horizontal velocity
            Velocity.X = RunSpeed * horizontalVelocityFactor * horizontalDirection;
        }

        private void UpdateJumpVelocity()
        {
            jumpButton.Update();

            if (!isJumpButtonPressed && IsTouchingTheGround && jumpButton.IsPressed())
            {
                Velocity.Y = JumpForce;
                IsTouchingTheGround = false;
                isJumpButtonPressed = true;
            }
            else if (isJumpButtonPressed && jumpButton.IsReleased())
            {
                if (Velocity.Y < 0)
                {
                    Velocity.Y *= 0.5f;
                }
                isJumpButtonPressed = false;
            }
        }

        protected override void UpdateStates()
        {
            base.UpdateStates();

            // Running
            if (MoveDirection == Direction.Left || MoveDirection == Direction.Right)
            {
                IsRunning = true;
            }
            else
            {
                IsRunning = false;
            }

            // Jump & Fall
            if (Velocity.Y > 0)
            {
                IsFalling = true;
                IsJumping = false;
            }
            else if (Velocity.Y < 0)
            {
                IsJumping = true;
                IsFalling = false;
            }
        }

        protected override void UpdateSprite()
        {
            base.UpdateSprite();

            if (sprite is not null && LookDirection != Direction.None)
            {
                sprite.Direction = LookDirection;
            }
        }

        /// <summary>
        /// Before ApplyMoveAndDetectCollisions
        /// Sets the vertical velocity to apply gravity to the player
        /// </summary>
        private void PlatformerPlayer_ApplyGravity()
        {
            if (IsTouchingTheGround)
            {
                Velocity.Y = 0;
            }

            if (IsTouchingTheCeiling)
            {
                IsTouchingTheCeiling = false;
                Velocity.Y *= -0.5f;
            }

            Velocity += ((PlatformerMap)map).Gravity * Weight;
        }

        /// <summary>
        /// Sets the state of touching the ground or ceiling when a vertical collision with a solid occurs
        /// </summary>
        /// <param name="side">Side of the vertical collision</param>
        private void PlatformerPlayer_SetGroundAndCeilingStates(CollisionSide side)
        {
            // Hit the ground
            IsTouchingTheGround = false;
            if (side == CollisionSide.Top)
            {
                IsTouchingTheGround = true;
                if (IsFalling)
                {
                    IsFalling = false;
                    wasOnTheGround = true;
                }
            }

            // Hit the ceiling
            IsTouchingTheCeiling = false;
            if (side == CollisionSide.Bottom)
            {
                IsTouchingTheCeiling = true;
            }
        }

        /// <summary>
        /// After ApplyMoveAndDetectCollisions
        /// Sets the state of leaving a platform and fall from it by running
        /// </summary>
        private void PlatformerPlayer_UpdatePlatformRideState()
        {
            if (!isIntersectingWithObstacle && wasOnTheGround && !IsJumping)
            {
                wasOnTheGround = false;
                IsFalling = true;
                IsTouchingTheGround = false;
            }
        }
    }
}
