﻿using Microsoft.Xna.Framework;

namespace SixteenBitNuts
{
    public abstract class Mover
    {
        public Vector2 Velocity = Vector2.Zero;

        protected delegate Vector2 MoveHandler(float t);
        protected MoveHandler? MoveFunction;

        public virtual void Update(GameTime time)
        {
            if (MoveFunction is not null)
            {
                Velocity = MoveFunction.Invoke((float)time.TotalGameTime.TotalMilliseconds);
            }
        }
    }
}
