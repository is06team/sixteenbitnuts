﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace SixteenBitNuts
{
    public class Entity : Actor
    {
        public string Name { get; private set; }
        public Vector2 Velocity;

        private readonly List<Mover> movers = new();

        public Entity(Map map, string name, Point? hitBoxSize = null) : base(map, hitBoxSize)
        {
            Name = name;
        }

        public override void Update(GameTime time)
        {
            base.Update(time);

            foreach (var mover in movers)
            {
                mover.Update(time);
            }

            ApplyVelocityFromMovers();
        }

        public void AddMover(Mover mover)
        {
            movers.Add(mover);
        }

        private void ApplyVelocityFromMovers()
        {
            foreach (var mover in movers)
            {
                MoveX(mover.Velocity.X, null);
                MoveY(mover.Velocity.Y, null);
            }
        }
    }
}
