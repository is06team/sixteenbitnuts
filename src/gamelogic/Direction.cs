﻿using Microsoft.Xna.Framework;
using System;

namespace SixteenBitNuts
{
    public static class Radians
    {
        public const float Right = 0;
        public const float TopRight = MathHelper.PiOver4;
        public const float Top = MathHelper.PiOver2;
        public const float TopLeft = 3 * MathHelper.PiOver4;
        public const float Left = MathHelper.Pi;
        public const float BottomLeft = 5 * MathHelper.PiOver4;
        public const float Bottom = 3 * MathHelper.PiOver2;
        public const float BottomRight = 7 * MathHelper.PiOver4;
    }

    public enum Direction : int
    {
        None = -1,
        Right = 0,
        TopRight = 1,
        Top = 2,
        TopLeft = 3,
        Left = 4,
        BottomLeft = 5,
        Bottom = 6,
        BottomRight = 7
    }

    public static class DirectionHelper
    {
        public static string Identifier(this Direction direction)
        {
            return direction switch
            {
                Direction.Left => "left",
                Direction.Right => "right",
                Direction.TopRight => "topright",
                Direction.Top => "top",
                Direction.TopLeft => "topleft",
                Direction.BottomLeft => "bottomleft",
                Direction.Bottom => "bottom",
                Direction.BottomRight => "bottomright",
                _ => "none",
            };
        }

        public static Direction Opposite(this Direction direction)
        {
            return direction switch
            {
                Direction.Left => Direction.Right,
                Direction.Right => Direction.Left,
                Direction.TopRight => Direction.BottomLeft,
                Direction.Top => Direction.Bottom,
                Direction.TopLeft => Direction.BottomRight,
                Direction.BottomLeft => Direction.TopRight,
                Direction.Bottom => Direction.Top,
                Direction.BottomRight => Direction.TopLeft,
                _ => Direction.None,
            };
        }

        public static Direction HorizontalRound(this Direction direction)
        {
            return direction switch
            {
                Direction.BottomLeft => Direction.Left,
                Direction.BottomRight => Direction.Right,
                Direction.TopLeft => Direction.Left,
                Direction.TopRight => Direction.Right,
                _ => direction,
            };
        }

        public static sbyte HorizontalSign(this Direction direction)
        {
            return direction switch
            {
                Direction.Right => 1,
                Direction.Left => -1,
                _ => 0,
            };
        }

        /// <summary>
        /// Retrive the radian value of a given direction, -1 if the direction is none
        /// </summary>
        /// <param name="direction">The direction</param>
        /// <returns>Radian value</returns>
        public static double? GetRadians(this Direction direction)
        {
            return direction switch
            {
                Direction.Left => Radians.Left,
                Direction.Right => Radians.Right,
                Direction.TopRight => Radians.TopRight,
                Direction.Top => Radians.Top,
                Direction.TopLeft => Radians.TopLeft,
                Direction.BottomLeft => Radians.BottomLeft,
                Direction.Bottom => Radians.Bottom,
                Direction.BottomRight => Radians.BottomRight,
                _ => null,
            };
        }

        /// <summary>
        /// Return corresponding normalized vector from a direction
        /// </summary>
        /// <param name="direction">Direction</param>
        /// <returns>Normalized vector</returns>
        public static Vector2 GetVector(this Direction direction)
        {
            return direction switch
            {
                Direction.Left => new Vector2(-1, 0),
                Direction.Right => new Vector2(1, 0),
                Direction.TopRight => new Vector2((float)Math.Cos(MathHelper.PiOver4), (float)Math.Sin(MathHelper.PiOver4)),
                Direction.Top => new Vector2(0, -1),
                Direction.TopLeft => new Vector2((float)Math.Cos(3 * MathHelper.PiOver4), (float)Math.Sin(3 * MathHelper.PiOver4)),
                Direction.BottomLeft => new Vector2((float)Math.Cos(5 * MathHelper.PiOver4), (float)Math.Sin(5 * MathHelper.PiOver4)),
                Direction.Bottom => new Vector2(0, 1),
                Direction.BottomRight => new Vector2((float)Math.Cos(7 * MathHelper.PiOver4), (float)Math.Sin(7 * MathHelper.PiOver4)),
                _ => Vector2.Zero,
            };
        }

        public static Direction FromNormalizedHorizontal(int value)
        {
            if (value == 1)
            {
                return Direction.Right;
            }
            else if (value == -1)
            {
                return Direction.Left;
            }
            return Direction.None;
        }

        public static Direction FromNormalizedVector(Vector2 value)
        {
            if (value.X == 1)
            {
                if (value.Y == 1)
                {
                    return Direction.BottomRight;
                }
                else if (value.Y == -1)
                {
                    return Direction.TopRight;
                }
                else
                {
                    return Direction.Right;
                }
            }
            else if (value.X == -1)
            {
                if (value.Y == 1)
                {
                    return Direction.BottomLeft;
                }
                else if (value.Y == -1)
                {
                    return Direction.TopLeft;
                }
                else
                {
                    return Direction.Left;
                }
            }
            else
            {
                if (value.Y == 1)
                {
                    return Direction.Bottom;
                }
                else if (value.Y == -1)
                {
                    return Direction.Top;
                }
                else
                {
                    return Direction.None;
                }
            }
        }
    }
}
