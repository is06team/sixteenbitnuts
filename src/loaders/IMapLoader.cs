﻿namespace SixteenBitNuts
{
    public interface IMapLoader
    {
        void LoadMapData(Map map, string name, bool shouldClearBinaryCache);
        void SetEntityFactory(EntityFactory factory);
    }
}
