﻿using Microsoft.Xna.Framework;
using System;

namespace SixteenBitNuts
{
    public static class VectorHelper
    {
        public static Vector2 FromAngle(float radians)
        {
            return new Vector2(
                (float)Math.Cos(radians),
                (float)Math.Sin(radians)
            );
        }

        public static float Radians(this Vector2 vector)
        {
            return 0;
        }
    }
}
